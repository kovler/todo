# from django.shortcuts import render
#
# # Create your views here.
# from rest_framework import viewsets
from django.shortcuts import render
from .models import Task
# from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt


def login(request):
    return render(request, './login.html', {})


def todo(request, name):
    tasks = Task.objects.filter(name=name)
    context = {"tasks": tasks}
    return render(request, './todo.html', context)


def mangal(request):
    tasks = Task.objects.filter(name='mangal')
    context = {"tasks": tasks}
    return render(request, './mangal.html', context)


@csrf_exempt
def post_task(request):
    name = request.POST["name"]
    content = request.POST["content"]
    new_task = Task(name=name, the_task=content)
    new_task.save()
    return render(request, './todo.html', {})


@csrf_exempt
def check_task(request):
    name = request.POST["name"]
    content = request.POST["content"]
    task = Task.objects.get(name=name, the_task=content)
    task.is_checked = True
    task.save()
    return render(request, './todo.html', {})


@csrf_exempt
def uncheck_task(request):
    name = request.POST["name"]
    content = request.POST["content"]
    task = Task.objects.get(name=name, the_task=content)
    task.is_checked = False
    task.save()
    return render(request, './todo.html', {})


@csrf_exempt
def delete_task(request):
    name = request.POST["name"]
    content = request.POST["content"]
    task = Task.objects.get(name=name, the_task=content)
    task.delete()
    return render(request, './todo.html', {})
