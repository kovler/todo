from django.conf.urls import url
import views




urlpatterns = [

    url(r'^$', views.login, name='login'),
    url(r'^todo/(?P<name>[a-z]+)/', views.todo, name='todo'),
    url(r'^post_task/', views.post_task, name='post_task'),
    url(r'^check/', views.check_task, name='check_task'),
    url(r'^un_check/', views.uncheck_task, name='uncheck_task'),
    url(r'^delete/', views.delete_task, name='delete_task')
]
