# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-05-08 19:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Todo', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='task',
            name='user',
        ),
        migrations.AddField(
            model_name='task',
            name='name',
            field=models.CharField(default=b'', max_length=50),
        ),
        migrations.DeleteModel(
            name='Username',
        ),
    ]
