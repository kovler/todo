/**
 * Created by User on 1/13/2016.
 */


$(document).ready(function () {
    var name = window.location["pathname"].split("/").slice(-2)[0];

    deleting(name);

    checking(name);

    add_task();

    cancel();

    save_task(name);

    logout()
});

function save_task(name) {
    $("#save_task").click(function () {

        $('#task_form').css("display", "none");
        var the_task = $("#the_task").val();

        var new_text = $("<span/>", {"class": "tasks_text"});
        var new_task = $("<div/>", {"class": "task"});
        var new_check_box = $("<div/>", {"class": "check_boxes"});

        new_task.append(new_check_box);
        new_text.append(the_task);
        new_task.append(new_text);
        var img = $('<img />', {
            class: 'trash',
            src: '/static/image/trash.jpg',
        });
        new_task.append(img);
        $("#tasks").append(new_task);

        $('#add_task').css('display', 'block');

        $.post('https://radiant-fjord-36695.herokuapp.com/post_task/', {"content": the_task, "name": name});

        $(".check_boxes").on('click', function () {
            var content = $(this).parent().children()[1].innerText;
            if ($(this)["context"].className === "check_boxes checked") {
                $.post('https://radiant-fjord-36695.herokuapp.com/un_check/', {"name": name, "content": content});
                $(this)["context"].className = 'check_boxes';
                $(this).children().remove()
            }
            else {
                var img = $('<img />', {
                    class: 'V-check',
                    src: '/static/image/V.png',
                });
                $(this).append(img);
                $(this)["context"].className = "check_boxes checked";
                $.post('https://radiant-fjord-36695.herokuapp.com/check/', {"name": name, "content": content});
            }
        });

        $('.trash').on("click", function () {
            var content = $(this).parent().children()[1].innerText;
            $.post('https://radiant-fjord-36695.herokuapp.com/delete/', {"name": name, "content": content});
            $(this).parent().remove()
        });

    });
}

function cancel() {
    $('#cancel_task').on('click', function () {
        $('#task_form').css('display', 'none')
        $('#add_task').css('display', 'block')
    });
}

function deleting(name) {
    $('.trash').on("click", function () {
        var content = $(this).parent().children()[1].innerText;
        $.post('https://radiant-fjord-36695.herokuapp.com/delete/', {"name": name, "content": content});
        $(this).parent().remove()
    });
}

function checking(name) {
    $(".check_boxes").on('click', function () {
        var content = $(this).parent().children()[1].innerText;
        if ($(this)["context"].className === "check_boxes checked") {
            $.post('https://radiant-fjord-36695.herokuapp.com/un_check/', {"name": name, "content": content});
            $(this)["context"].className = 'check_boxes';
            $(this).children().remove()
        }
        else {
            var img = $('<img />', {
                class: 'V-check',
                src: '/static/image/V.png',
            });
            $(this).append(img);
            $(this)["context"].className = "check_boxes checked";
            $.post('https://radiant-fjord-36695.herokuapp.com/check/', {"name": name, "content": content});
        }
    });
}

function add_task() {
    $('#add_task').on('click', function () {
        $('#add_task').css("display", "none");
        $('#task_form').css('display', 'inline-flex');
    });
}

function logout() {
    $("#logout").on('click', function () {
        window.location.replace("https://radiant-fjord-36695.herokuapp.com");
    })
}


