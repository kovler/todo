from django.db import models
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class Task(models.Model):
    name = models.CharField(default='', max_length=50)
    the_task = models.CharField(default='', max_length=200)
    is_checked = models.BooleanField(default=False)
    bringer = models.CharField(default='', max_length=200)

    def __str__(self):
        return self.the_task + ' by ' + self.name
